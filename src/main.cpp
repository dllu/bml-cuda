/*
This file is part of bml-cuda by Daniel Lu
Copyright (C) 2013  Daniel Lu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cstdio>
#include <ctime>
#include "bml.h"
int main(){
    int xsize = 1024, ysize = 1024;
    int limit = 1000000;
    long long start = clock();
    for(double percent = 0.28; percent < 0.45; percent +=0.01){
        for(int j=0; j<10; j++){
	        srand ( clock() );
	        bml test(xsize,ysize,percent);
            int numcars = test.population();
            printf("Population: %d, %f\n", numcars, percent);

            char mobilityfilename[200];
            sprintf(mobilityfilename, "mobility_%d_%f_%d.txt", numcars, percent, j);
            FILE * mobilities = fopen(mobilityfilename,"w");

            char filename[200];
            sprintf(filename,"bml_%d_%f_%d_start",numcars, percent, j);
            test.bmp(filename);

	        for(int i=0; i<limit; i++){
                if(i<limit-10000 && i%256 != 0) {
                    test.run();
                } else {
                    int v = test.speed();
                    /*printf("%d\n",v);
                    char filename2[200];
                    sprintf(filename2,"run %d %d",i,v);
                    test.bmp(filename2);*/
                    if(i>=limit-10000) fprintf(mobilities, "%d ", v);
                    if(v <= 0 || v >= numcars) {
                        printf("Simulation ended: %d\n", i);
                        break;
                    }
                }
	        }
            fprintf(mobilities, "\n");
            char filename2[200];
            sprintf(filename2,"bml_%d_%f_%d_stop_%d", numcars, percent, j, test.speed());
            test.bmp(filename2);
            fclose(mobilities);
            fflush(stdout);
        }
    }
    long long stop = clock();
    printf("Calculation done. time elapsed: %f\n", (stop-start)/(double)CLOCKS_PER_SEC);
	return 0;
}
